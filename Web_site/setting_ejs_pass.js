
var ejs_pass = {};

ejs_pass.init = function(app){
    
    console.log('ejs 관련 위치 설정을 시작합니다.');
    
    pass_acting(app);
    
}

function pass_acting (app){
    
    app.get("/",function(req,res){
        console.log('테스트 페이지!');
        
        var context = {userId:null,userName:null};
        
        if(req.session.user){
            console.log(req.session.user.id);
            console.log(req.session.user.name);
            
            context = {userId : req.session.user.id,userName : req.session.user.name};
        }
        
        res.render("index",context);
    });
    
    app.get("/national_travel",function(req,res){
        console.log('국내 여행 페이지!');
        
        var context = {userId:null,userName:null};
        
        if(req.session.user){
            console.log(req.session.user.id);
            console.log(req.session.user.name);

            context = {userId : req.session.user.id,userName : req.session.user.name};
        }
        
        res.render("national_travel",context);
    });
    
    app.get("/vehicle_travel",function(req,res){
        console.log('여행 교통수단 페이지!');
        
        var context = {userId:null,userName:null};
        
        if(req.session.user){
            console.log(req.session.user.id);
            console.log(req.session.user.name);

            context = {userId : req.session.user.id,userName : req.session.user.name};
        }
        
        res.render("vehicle_travel",context);
    });
    
    app.get("/ready_to_travel",function(req,res){
        console.log("여행 준비 페이지!");
        
        var context = {userId:null,userName:null};
        
        if(req.session.user){
            console.log(req.session.user.id);
            console.log(req.session.user.name);

            context = {userId : req.session.user.id,userName : req.session.user.name};
        }
        
        res.render("ready_to_travel",context);
    });
    
    app.get("/test_making",function(req,res){
        console.log("페이지 제작 중!");
    
        res.render("test1",{});
    });
    
    app.get("/update_post",function(req,res){
        console.log("게시글 수정 페이지!");
        
        res.render("update_post",{});
    });
    
    app.get("/upload_page",function(req,res){
        console.log("사진 업데이트");
        
        res.render("upload_page",{});
    });
    
}


module.exports = ejs_pass;