
var multer = require('multer');

var setting_multer = {};

setting_multer.init = function(app,path){
    
    console.log('multer 관련 정보를 설정합니다.');
    
    setting_multer_acting(app,path);
}

function setting_multer_acting (app,path){
    
    const upload = multer({
    storage: multer.diskStorage({
        destination: function(req,file,cb){
            cb(null,'uploads/');
        },
        filename: function(req,file,cb){
                if(file == undefined){
                    cb(null,null);
                }
                else
                {
                    cb(null,new Date().valueOf() + path.extname(file.originalname));
                }
            }
        }),
    });
    
    app.set('upload',upload);
    
};

module.exports = setting_multer;