

var addpost = function(req,res){
    console.log('post 모듈 안에 있는 addpost 호출됨');
    
    var paramTitle = req.body.title || req.query.title;
    var paramContents = req.body.contents || req.query.contents;
    var paramWriter = req.body.writer || req.query.writer;
    var paramInfo = req.body.info || req.query.info;
    var paramRegion = req.body.region || req.query.region;
    var paramPhoto =null;
    var paramTraffic = req.body.traffic || req.query.traffic;
    
    if(req.file == null){
        paramPhoto = 'uploads\\2222628200.png'
    }else{
        paramPhoto = req.file.path;
    }
        
    paramContents = paramContents.replace('<p>','');
    paramContents = paramContents.replace('</p>','');
    
    console.log('요청 파라미터 : '+paramTitle+', '+paramContents+', '+paramWriter+', '+paramInfo+','+paramRegion+','+paramPhoto+','+paramTraffic);
    
    var database = req.app.get('database');
    
    if(database.db){
        database.UserModel.findById(paramWriter,function(err,results){
            if(err){
                console.error('게시판 글 추가 중 오류 발생 : '+err.stack);
                
                res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                res.write('<h2>게시판 글 추가 중 오류 발생</h2>');
                res.write('<p>'+err.stack+'</p>');
                res.end();
                
                return;
            }
            
            if(results === undefined || results.length <1){
                res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                res.write('<h2>사용자 ['+paramWriter+']를 찾을 수 없습니다.</h2>');
                res.end();
                
                return;
            }
            
            var userObjectId = results[0]._doc._id;
            console.log('사용자 ObjectId : '+paramWriter+' -> '+userObjectId);
            
            var post1 = new database.PostModel({
                title : paramTitle,
                contents : paramContents,
                writer : userObjectId,
                info : paramInfo,
                region : paramRegion,
                photo : paramPhoto,
                traffic: paramTraffic
            });
            
            post1.savePost(function(err,result){
                if(err) {throw err;}
                
                console.log("글 데이터 추가함.");
                console.log('글 작성','포스트 글을 생성했습니다. : '+post1._id);
                
                return res.redirect('/process/showpost/'+post1._id);
            });
            
        });
    }
};

var listpost = function(req,res){
    
    console.log('post 모듈 안에 있는 listpost 호출됨.');
    
    var paramPage = req.body.page || req.query.page;
    var paramPerPage = req.body.perPage || req.query.perPage;
    
    console.log('요청 파라미터 : '+paramPage+', '+paramPerPage);
    
    var database = req.app.get('database');
    
    if(database.db){
        
        var options = {
            page: paramPage,
            perPage: paramPerPage
        }
        
        
        database.PostModel.list(options,function(err,results){
            if(err){
                console.error('게시판 글 목록 조회 중 오류 발생 : '+err.stack);
                res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                res.write('<h2>게시판 글 목록 조회 중 오류 발생</h2>');
                res.write('<p>'+err.stack+'</p>');
                res.end();
                
                return;
            }
            
            if(results){
                console.dir(results);
                
                database.PostModel.count().exec(function(err,count){
                    res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                    
                    var context = {
                        title : '글 목록',
                        posts : results,
                        page : parseInt(paramPage),
                        pageCount : Math.ceil(count / paramPerPage),
                        perPage : paramPerPage,
                        totalRecords : count,
                        size : paramPerPage
                    };
                    
                    console.dir(context);
                    console.log(count);
                    
                    req.app.render('route_response/listpost',context,function(err,html){
                        if(err){
                            console.error('응답 웹문서 생성 중 오류 발생 : '+err.stack);
                            
                            res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                            res.write('<h2>응답 웹문서 생성 중 오류 발생</h2>');
                            res.write('<p>'+err.stack+'</p>');
                            res.end();
                            
                            return;
                        }
                        
                        res.end(html);
                    });
                });
            }
        });
    }
    
};

var showpost = function(req,res){
    console.log('post 모듈 안에 있는 showpost 호출됨.');
    
    var paramId = req.body.id || req.query.id || req.params.id;
    
    console.log('요청 파라미터 : '+paramId);
    
    var database = req.app.get('database');
    
    if(database.db){
        database.PostModel.load(paramId,function(err,results){
            if(err){
                console.error('게시판 글 조회 중 오류 발생 : '+err.stack);
                
                res.writeHead('200',{'Content-Type':'text/html; charset=utf8'});
                res.wrtie('<h2>게시판 글 조회 중 오류 발생</h2>');
                res.write('<p>'+err.stack+'</p>');
                res.end();
                
                return;
            }
            
            if(results){
                console.dir(results);
                res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                
                var context = {
                    title : '글 조회',
                    posts : results,
                    //Entities : Entities
                };
                
                req.app.render('route_response/showpost',context,function(err,html){
                    if(err){throw err;}
                    console.log('응답 웹 문서 : '+html);
                    res.end(html);
                });
            }
        });
    }
};

var deletepost = function(req,res){
    console.log('post 모듈 안에 있는 deletepost 호출됨');
    
    var paramTitle = req.body.title || req.query.title;
    var paramWriter = req.body.writer || req.query.writer;
    
    console.log('요청 파라미터 :'+paramTitle+', '+paramWriter);
    
    var database = req.app.get('database');
    
    if(database.db){
        database.UserModel.findById(paramWriter,function(err,result){
            if(err){
                console.err('게시판 글 삭제 중 오류 발생:'+err.stack);
                
                res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                res.write('<h2>게시판 글 삭제 중 오류 발생</h2>');
                res.write('<p>'+err.stack+'</p>');
                res.end();
                
                return;
            }
            
            if(result== undefined || result.length<1){
                res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                res.write('<h2>사용자['+paramWriter+']를 찾을 수 없습니다.</h2>');
                res.end();
                
                return;
            }
            
            var userObjectId = result[0]._doc._id;
            console.log('사용자 ObjectId :'+paramWriter+'->'+userObjectId);
            
            database.PostModel.delete(paramTitle,userObjectId,function(err,results){
                if(err){
                    console.error('게시판 글 삭제 중 오류 발생: '+err.stack);
                    
                    res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                    res.write('<h2>게시판 글 삭제 중 오류 발생</h2>');
                    res.write('<p>'+err.stack+'</p>');
                    res.end()
                    
                    return;
                }
                
                if(result){
                    console.dir(result);
                    
                    res.writeHead('200',{'Content-Type':'text/html;charset:utf8'});
                    
                    var context = {title:'게시판 내용 삭제 성공'};
                    
                    req.app.render('route_response/deletepost',context,function(err,html){
                        if(err){
                            console.error('뷰 렌더링 중 오류 발생 :'+err.stack);
                            
                            res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                            res.write('<h2>뷰 렌더링 중 오류 발생</h2>');
                            res.write('<p>'+err.stack+'</p>');
                            res.end();
                            
                            return;
                        }
                        
                        console.log('rendered: '+html);
                        
                        res.end(html);
                    });   
                }
            });
            
        });
    }else{
        res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
        res.write('<h2>데이터베이스 연결 실패</h2>');
        res.end();
    } 
};

var updatepost = function(req,res){
    console.log('post 모듈 안에 있는 updatepost 호출됨');
    
    var paramTitle = req.body.title || req.query.title;
    var paramContents = req.body.contents || req.guery.contents;
    var paramWriter = req.body.writer || req.query.writer;
    var paramInfo = req.body.info || req.query.info;
    var paramObjectId = req.body.objectId || req.query.objectId;
    var paramRegion = req.body.region || req.query.region;
    var paramPhoto = null;
    var paramTraffic = req.body.traffic || req.query.traffic;
    
    if(req.file == null){
        paramPhoto = 'uploads\\2222628200.png'
    }else{
        paramPhoto = req.file.path;
    }
    
    
    paramContents = paramContents.replace('<p>','');
    paramContents = paramContents.replace('</p>','');
    
    
    console.log('해당 값들 확인: Title=>'+paramTitle+', Writer=>'+paramWriter+', Contents=>'+paramContents+', Info=>'+paramInfo+', ObjectId=>'+paramObjectId);
    
    var database = req.app.get('database');
    
    if(database.db){
        database.PostModel.update(paramTitle,paramContents,paramInfo,paramObjectId,paramRegion,paramPhoto,paramTraffic,function(err,results){
            if(err){
                console.error('게시판 글 수정 중 오류 발생 : '+err.stack);
                
                res.writeHead('200',{'Content-Type':'text/html; charset=utf8'});
                res.wrtie('<h2>게시판 글 조회 중 오류 발생</h2>');
                res.write('<p>'+err.stack+'</p>');
                res.end();
                
                return;
            }
            
            if(results){
                console.dir(results);
                res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                
                var context = {
                    title : '글 수정를 완료했습니다.',
                    posts : results,
                    contents_after :paramContents,
                    //Entities : Entities
                };
                
                req.app.render('route_response/update_showpost',context,function(err,html){
                    if(err){throw err;}
                    console.log('응답 웹 문서 : '+html);
                    res.end(html);
                });
            }
            
        });
        
    }
    
};

var acting_region = function(req,res){
    
    console.log('post 모듈 안에 있는 /process/acting_region 호출됨');
    
    var paramRegion = req.body.region || req.query.region;
    var paramPage = req.body.page || req.query.page;
    var paramPerPage = req.body.perPage || req.query.perPage;
    
    console.log('Post Parameter = '+paramRegion+' page = '+paramPage+' perPage = '+paramPerPage);
    
    var database = req.app.get('database');
    
    if(database.db){
        
        var options = {
            page: paramPage,
            perPage: paramPerPage
        }
        
        database.PostModel.list(options,function(err,results){
            if(err){
                console.error('게시판 글 목록 조회 중 오류 발생 :'+err.stack);
                
                res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                res.write('<h2>게시판 글 목록 조회 중 오류 발생</h2>');
                res.write('<p>'+err.stack+'</p>');
                res.end();
                
                return;
            }
            
            if(results){
                
                console.dir(results);
                
                var data_list = [];
                
                for(var i =0; i<results.length; i++){
                    var find_region = results[i].region;
                    
                    const words = find_region.split(' ');
                    console.log('찾은 지역:'+words[0]);
                    
                    if(words[0] == paramRegion) { 
                        console.log(results[i]);
                        data_list.push(results[i]);
                    }
                    
                }
                
                console.log(data_list);
                
                database.PostModel.count().exec(function(err,count){
                    res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                    
                    var re_count = count-(count-data_list.length);
                    
                    var context = {
                        title: '글 목록',
                        posts: data_list,
                        page: parseInt(paramPage),
                        pageCount: Math.ceil(re_count/paramPerPage),
                        perPage: paramPerPage,
                        totalRecords: re_count,
                        size: paramPerPage,
                        post:paramRegion
                    };
                    
                    console.dir(context);
                    console.log(re_count);
                    
                req.app.render('route_response/listpost_change',context,function(err,html){
                        if(err){
                            console.error('응답 웹문서 생성 중 오류 발생 : '+err.stack);
                            
                            res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                            res.write('<h2>응답 웹문서 생성 중 오류 발생</h2>');
                            res.write('<p>'+err.stack+'</p>');
                            res.end();
                            
                            return;
                        }
                        res.end(html);
                    });
                });
                
            }  
        });
    }  
};

var acting_vehicle = function(req,res){
    
    console.log('post 모듈 안에 있는 /process/acting_vehicle 호출됨');
    
    var paramvehicle = req.body.vehicle || req.query.vehicle;
    var paramPage = req.body.page || req.query.page;
    var paramPerPage = req.body.perPage || req.query.perPage;
    
    console.log('Post Parameter = '+paramvehicle+' page = '+paramPage+' perPage = '+paramPerPage);
    
    var database = req.app.get('database');
    
    if(database.db){
        
        var options = {
            page: paramPage,
            perPage: paramPerPage
        }
        
        database.PostModel.list(options,function(err,results){
            if(err){
                console.error('게시판 글 목록 조회 중 오류 발생 :'+err.stack);
                
                res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                res.write('<h2>게시판 글 목록 조회 중 오류 발생</h2>');
                res.write('<p>'+err.stack+'</p>');
                res.end();
                
                return;
            }
            
            if(results){
                
                console.dir(results);
                
                var data_list = [];
                
                for(var i =0; i<results.length; i++){
                    var find_vehicle = results[i].traffic;
                    
                    //const words = find_region.split(' ');
                    //console.log('찾은 지역:'+words[0]);
                    console.log('찾은 교통수단:'+find_vehicle)
                    
                    if(find_vehicle == paramvehicle) { 
                        console.log(results[i]);
                        data_list.push(results[i]);
                    }
                    
                }
                
                console.log(data_list);
                
                database.PostModel.count().exec(function(err,count){
                    res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                    
                    var re_count = count-(count-data_list.length);
                    
                    var context = {
                        title: '글 목록',
                        posts: data_list,
                        page: parseInt(paramPage),
                        pageCount: Math.ceil(re_count/paramPerPage),
                        perPage: paramPerPage,
                        totalRecords: re_count,
                        size: paramPerPage,
                        post:paramvehicle
                    };
                    
                    console.dir(context);
                    console.log(re_count);
                    
                req.app.render('route_response/listpost_change',context,function(err,html){
                        if(err){
                            console.error('응답 웹문서 생성 중 오류 발생 : '+err.stack);
                            
                            res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                            res.write('<h2>응답 웹문서 생성 중 오류 발생</h2>');
                            res.write('<p>'+err.stack+'</p>');
                            res.end();
                            
                            return;
                        }
                        res.end(html);
                    });
                });
                
            }  
        });
    }  
    
};

var acting_ready_to_travel = function(req,res){
    
    console.log('post 모듈 안에 있는 /process/acting_ready_to_travel 호출됨');
    
    var paramReady_to_travel = req.body.ready_to_travel || req.query.ready_to_travel;
    var paramPage = req.body.page || req.query.page;
    var paramPerPage = req.body.perPage || req.query.perPage;
    
    console.log('Post Parameter = '+paramReady_to_travel+' page = '+paramPage+' perPage = '+paramPerPage);
    
    var database = req.app.get('database');
    
    if(database.db){
        
        var options = {
            page: paramPage,
            perPage: paramPerPage
        }
        
        database.PostModel.list(options,function(err,results){
            if(err){
                console.error('게시판 글 목록 조회 중 오류 발생 :'+err.stack);
                
                res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                res.write('<h2>게시판 글 목록 조회 중 오류 발생</h2>');
                res.write('<p>'+err.stack+'</p>');
                res.end();
                
                return;
            }
            
            if(results){
                
                console.dir(results);
                
                var data_list = [];
                
                for(var i =0; i<results.length; i++){
                    var find_ready_to_travel = results[i].info;
                    
                    //const words = find_region.split(' ');
                    //console.log('찾은 지역:'+words[0]);
                    
                    console.log('찾은 종류 여행:'+find_ready_to_travel);
                    
                    if(find_ready_to_travel == paramReady_to_travel) { 
                        console.log(results[i]);
                        data_list.push(results[i]);
                    }
                    
                }
                
                console.log(data_list);
                
                database.PostModel.count().exec(function(err,count){
                    res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                    
                    var re_count = count-(count-data_list.length);
                    
                    var context = {
                        title: '글 목록',
                        posts: data_list,
                        page: parseInt(paramPage),
                        pageCount: Math.ceil(re_count/paramPerPage),
                        perPage: paramPerPage,
                        totalRecords: re_count,
                        size: paramPerPage,
                        post:paramReady_to_travel
                    };
                    
                    console.dir(context);
                    console.log(re_count);
                    
                req.app.render('route_response/listpost_change',context,function(err,html){
                        if(err){
                            console.error('응답 웹문서 생성 중 오류 발생 : '+err.stack);
                            
                            res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                            res.write('<h2>응답 웹문서 생성 중 오류 발생</h2>');
                            res.write('<p>'+err.stack+'</p>');
                            res.end();
                            
                            return;
                        }
                        res.end(html);
                    });
                });
                
            }  
        });
    }  
    
};

var acting_check_button = function(req,res){
    
    console.log('post 모듈 안에 있는 /process/acting_check_button 호출됨');
    
    var paramStory = req.body.story || req.query.story;
    var paramPage = req.body.page || req.query.page;
    var paramPerPage = req.body.perPage || req.query.perPage;
    
    console.log('Post Parameter: story = '+paramStory+' page = '+paramPage+' perPage = '+paramPerPage);
    
    var database = req.app.get('database');
    
    if(database.db){
        var options = {
            page: paramPage,
            perPage: paramPerPage
        }
    }
    
    database.PostModel.list(options,function(err,results){
        if(err){
            console.error('게시판 글 목록 조회 중 오류 발생 :'+err.stack);

            res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
            res.write('<h2>게시판 글 목록 조회 중 오류 발생</h2>');
            res.write('<p>'+err.stack+'</p>');
            res.end();

            return; 
        }
        
       if(results){
           console.dir(results);
                
                var data_list = [];
                
                for(var i =0; i<results.length; i++){
                    var find_region = results[i].region;
                    
                    //const words = find_region.split(' ');
                    //console.log('찾은 지역:'+words[0]);
                    
                    console.log('찾은 종류 여행:'+find_region);
                    
                    if(find_region == paramStory) { 
                        console.log(results[i]);
                        data_list.push(results[i]);
                    }
                    
                }
                
                console.log(data_list);
                
                database.PostModel.count().exec(function(err,count){
                    res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                    
                    var re_count = count-(count-data_list.length);
                    
                    var context = {
                        title: '글 목록',
                        posts: data_list,
                        page: parseInt(paramPage),
                        pageCount: Math.ceil(re_count/paramPerPage),
                        perPage: paramPerPage,
                        totalRecords: re_count,
                        size: paramPerPage,
                        post:paramStory
                    };
                    
                    console.dir(context);
                    console.log(re_count);
                    
                req.app.render('route_response/listpost_change',context,function(err,html){
                        if(err){
                            console.error('응답 웹문서 생성 중 오류 발생 : '+err.stack);
                            
                            res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                            res.write('<h2>응답 웹문서 생성 중 오류 발생</h2>');
                            res.write('<p>'+err.stack+'</p>');
                            res.end();
                            
                            return;
                        }
                        res.end(html);
                    });
                });
            }  
       }); 
};

var acting_vehicle_check_button = function(req,res){
    console.log('post 모듈 안에 있는 /process/acting_check_button 호출됨');
    
    var paramStory = req.body.story || req.query.story;
    var paramPage = req.body.page || req.query.page;
    var paramPerPage = req.body.perPage || req.query.perPage;
    
    console.log('Post Parameter: story = '+paramStory+' page = '+paramPage+' perPage = '+paramPerPage);
    
    var database = req.app.get('database');
    
    if(database.db){
        var options = {
            page: paramPage,
            perPage: paramPerPage
        }
    }
    
    database.PostModel.list(options,function(err,results){
        if(err){
            console.error('게시판 글 목록 조회 중 오류 발생 :'+err.stack);

            res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
            res.write('<h2>게시판 글 목록 조회 중 오류 발생</h2>');
            res.write('<p>'+err.stack+'</p>');
            res.end();

            return; 
        }
        
       if(results){
           console.dir(results);
                
                var data_list = [];
                
                for(var i =0; i<results.length; i++){
                    var find_traffic = results[i].traffic;
                    
                    //const words = find_region.split(' ');
                    //console.log('찾은 지역:'+words[0]);
                    
                    console.log('찾은 종류 여행:'+find_traffic);
                    
                    if(find_traffic == paramStory) { 
                        console.log(results[i]);
                        data_list.push(results[i]);
                    }
                    
                }
                
                console.log(data_list);
                
                database.PostModel.count().exec(function(err,count){
                    res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                    
                    var re_count = count-(count-data_list.length);
                    
                    var context = {
                        title: '글 목록',
                        posts: data_list,
                        page: parseInt(paramPage),
                        pageCount: Math.ceil(re_count/paramPerPage),
                        perPage: paramPerPage,
                        totalRecords: re_count,
                        size: paramPerPage,
                        post:paramStory
                    };
                    
                    console.dir(context);
                    console.log(re_count);
                    
                req.app.render('route_response/listpost_change',context,function(err,html){
                        if(err){
                            console.error('응답 웹문서 생성 중 오류 발생 : '+err.stack);
                            
                            res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                            res.write('<h2>응답 웹문서 생성 중 오류 발생</h2>');
                            res.write('<p>'+err.stack+'</p>');
                            res.end();
                            
                            return;
                        }
                        res.end(html);
                    });
                });
            }  
       }); 
};

var acting_search_word = function(req,res){
    console.log('post 모듈 안에 있는 /process/acting_search_word 호출됨');
    
    var paramSearch_word = req.body.search_word || req.query.search_word;
    var paramPage = req.body.page || req.query.page;
    var paramPerPage = req.body.perPage || req.query.perPage;
    
    console.log('Post Parameter: paramSearch_word = '+paramSearch_word+' page = '+paramPage+' perPage = '+paramPerPage);
    
    var database = req.app.get('database');
    
    if(database.db){
        var options = {
            page: paramPage,
            perPage: paramPerPage
        }
    }
    
    database.PostModel.list(options,function(err,results){
        if(err){
            console.error('게시판 글 목록 조회 중 오류 발생 :'+err.stack);

            res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
            res.write('<h2>게시판 글 목록 조회 중 오류 발생</h2>');
            res.write('<p>'+err.stack+'</p>');
            res.end();

            return; 
        }
        
       if(results){
           console.dir(results);
                
                var data_list = [];
                
                for(var i =0; i<results.length; i++){
                    var find_contents = results[i].contents;
                    
                    //const words = find_region.split(' ');
                    //console.log('찾은 지역:'+words[0]);
                    
                    console.log('내용 조회:'+find_contents);
                    const words = find_contents.split(' ');
                    console.dir('여기까지 완료:'+words)
                    
                    for(var j=0; j<words.length; j++){
                        
                        if(words[j] == paramSearch_word){
                            console.log(results[i]);
                            data_list.push(results[i]);
                        }
                    }
                }
                
                console.log(data_list);
                
                database.PostModel.count().exec(function(err,count){
                    res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                    
                    var re_count = count-(count-data_list.length);
                    
                    var context = {
                        title: '글 목록',
                        posts: data_list,
                        page: parseInt(paramPage),
                        pageCount: Math.ceil(re_count/paramPerPage),
                        perPage: paramPerPage,
                        totalRecords: re_count,
                        size: paramPerPage,
                        post:paramSearch_word
                    };
                    
                    console.dir(context);
                    console.log(re_count);
                    
                req.app.render('route_response/listpost_change',context,function(err,html){
                        if(err){
                            console.error('응답 웹문서 생성 중 오류 발생 : '+err.stack);
                            
                            res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                            res.write('<h2>응답 웹문서 생성 중 오류 발생</h2>');
                            res.write('<p>'+err.stack+'</p>');
                            res.end();
                            
                            return;
                        }
                        res.end(html);
                    });
                });
            }  
       }); 
};


module.exports.addpost = addpost;
module.exports.listpost = listpost;
module.exports.showpost = showpost;
module.exports.deletepost = deletepost;
module.exports.updatepost = updatepost;
module.exports.acting_region = acting_region;
module.exports.acting_vehicle = acting_vehicle;
module.exports.acting_ready_to_travel = acting_ready_to_travel;
module.exports.acting_check_button = acting_check_button;
module.exports.acting_vehicle_check_button = acting_vehicle_check_button;
module.exports.acting_search_word = acting_search_word;