

var ajax_response = function(req,res){
    console.log('/process/ajax_response 호출됨');
    
    var data = req.body.data;
    
    console.log('Post Parameter = '+data);
    
    var result = data + ' Success';
    
    console.log(result);
    
    res.send({result:result});
};

var ajax_random_img = function(req,res){
    console.log('/process/ajax_random_img 호출됨');
    
    var data = req.body.data;
    
    console.log('Post Parameter = '+data);
    
    var result = data +' Success';
    
    res.send({result:result});
};

var ajax_sample = function(req,res){
    console.log('/process/ajax_sample 호출됨');
    
    var data = [{"name":"갑돌이","age":19},{"name":"갑순이","age":20},{"name":"갑을이","age":21}];
    
    console.log('Post Parameter = '+data);
    
    var result = data;
    
    res.send({result:result});
};

var ajax_logout = function(req,res){
    
    console.log('/process/ajax_logout 호출됨');

    var data = {};
    
    if(req.session.user){
        console.log('로그아웃합니다.');
        
        data = {id:req.session.user.id,name:req.session.user.name};
        
        console.log(data);
        
        req.session.destroy(function(err){
            if(err){return err;}
            
            console.log('세션을 삭제하고 로그아웃되었습니다.');
            
            var result = data;
            
            res.send({result:result});
            
        });
    }else{
        console.log('아직 로그인되어 있지 않습니다.');
        
        data = {id:null,name:null};
        
        var result = data;
        
        res.send({result:result});
        
    }  
};

var ajax_exist_session = function(req,res){
    
    console.log('/process/ajax_exist_session 호출됨');

    var data = {};
    
    if(req.session.user){
        console.log('세션정보가 확인되었습니다.');
        
        data = {id:req.session.user.id,name:req.session.user.name};
        
        console.log(data);
        
        var result = data;
        
        res.send({result:result});
        
    }else{
        console.log('세션정보가 없습니다.');
        
        data = {id:null,name:null};
        
        var result = data;
        
        res.send({result:result});
        
    }  
    
};

var ajax_comment_response = function(req,res){
    
    console.log('/process/ajax_comment_response 호출됨');
    
    var objectId = req.body.objectId;
    
    var comment_data = req.body.data;
    
    var title = req.body.title;
    
    var session_data = {};
    
    console.log('받은 정보: objectId : '+objectId+ ' comment_data: '+ comment_data+' title: '+title+' 가 확인되었습니다.');
    
    if(req.session.user){
        console.log("세선정보가 확인되었습니다.");
        
        session_data = {id:req.session.user.id,name:req.session.user.name};
        
        console.log(session_data);
        
    }else{
        console.log("세선정보가 확인되지 않았습니다..");
        
        session_data = {id:null,name:null};
        
        console.log(session_data);
    }
    
    //var results = {"objectId":objectId,"comment_data":comment_data,"title":title,"session_id":session_data.id,"sesion_name":session_data.name};
    
    //console.dir(results);
    
    //res.send({results:results});
    
    var user_info = null;
    
    var post_result = null;
    
    var database = req.app.get('database');
    
    if(database.db){
        
        database.UserModel.findById(session_data.id,function(err,results){
            if(err){
                console.error('코멘트 추가 중 아이디 찾기 실패 : '+err.stack);
                                
                res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                res.write('<h2>코멘트 추가 중 오류 발생</h2>');
                res.write('<p>'+err.stack+'</p>');
                res.end();
                
                return;
            }
            
            if(results === undefined || results.length <1){
                res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                res.write('<h2>사용자 ['+paramWriter+']를 찾을 수 없습니다.</h2>');
                res.end();
                
                return;
            }
            
            var user_info = results[0];
            
            console.log('사용자 정보 찾음 : '+user_info);
            
            
            database.PostModel.load(objectId,function(err,results){
                
                if(err){
                    console.log("게시판 글 조회 중 오류 발생 :"+err.stack);

                    res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                    res.write('<h2>게시판 글 조회 중 오류 발생</h2>');
                    res.write('<p>'+err.stack+'</p>');
                    res.end();

                    return;
                }

                if(results){
                    console.log("포스트 게시글 정보 받음 : "+results);
                    post_result = results;
                    console.log('여기까지는 왔음! :'+post_result);
                    
                    console.log('처리할 준비!!!');
        
                    var comment = {contents:comment_data};

                    post_result.addComment(user_info,comment,function(err,result){
                        if(err){throw err;}

                        console.log("게시글의 코멘트 추가함.");
                        console.log("코멘트 생성",'코멘트를 생성했습니다. :'+post_result._id);

                        res.send({reaction:"코멘트 생성이 완료되었습니다."});

                    }); 
                } 
            });   
        });
    } 
};

var ajax_comment_total_check = function(req,res){
    
    console.log('/process/ajax_comment_total_check 호출됨');
    
    var objectId = req.body.objectId;
    
    console.log('받은 정보: objectId :'+objectId);
    
    var post_result =null;
    
    var database = req.app.get('database');
    
    if(database.db){
        database.PostModel.load(objectId,function(err,results){
            if(err){
                console.log("게시판 글 조회 중 오류 발생 :"+err.stack);

                res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                res.write('<h2>게시판 글 조회 중 오류 발생</h2>');
                res.write('<p>'+err.stack+'</p>');
                res.end();

                return;
            } 
            if(results){
                console.log('포스트 게시글 정보 받음: '+results);
                post_result = results;
                console.log('게시글 속 코멘트 정보 축출: '+post_result.comments);
                res.send({results:post_result.comments});
            }
        });  
    }
};

var ajax_comment_delete = function(req,res){
    
    console.log('/process/ajax_comment_delete 호출됨');
    
    var objectId = req.body.objectId;
    var writer = req.body.writer;
    var contents = req.body.contents;
    
    var data={};
    
    console.log('받은 정보: objectId :'+objectId+', writer :'+writer+', contents :'+contents);
    
    
    if(req.session.user){
        console.log('세션정보가 확인되었습니다.');
        
         data = {id:req.session.user.id,name:req.session.user.name};
        
        console.log(data);
        
    }else{
        console.log('세션정보가 없습니다.');
             
        res.send({reaction:'세션정보가 없습니다. 로그인를 해주세요!'});
        
        return;
        
    }
    
    var post_result =null;
    
    var database = req.app.get('database');
    
    if(database.db){
        
        database.UserModel.findById(data.id,function(err,results){
            if(err){
                console.error('코멘트 추가 중 아이디 찾기 실패 : '+err.stack);
                                
                res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                res.write('<h2>코멘트 추가 중 오류 발생</h2>');
                res.write('<p>'+err.stack+'</p>');
                res.end();
                
                return;
            }
            
            if(results === undefined || results.length <1){
                res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                res.write('<h2>사용자 ['+paramWriter+']를 찾을 수 없습니다.</h2>');
                res.end();
                
                return;
            }
            
            var user_object_id = results[0]._doc._id;
            console.log('user_object_id 확인 :'+user_object_id);
            
            database.PostModel.load(objectId,function(err,results){
               if(err){
                    console.log("게시판 글 조회 중 오류 발생 :"+err.stack);

                    res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                    res.write('<h2>게시판 글 조회 중 오류 발생</h2>');
                    res.write('<p>'+err.stack+'</p>');
                    res.end();

                    return;
                } 
                if(results){
                    console.log('포스트 게시글 정보 받음: '+results);
                    post_result = results;
                    
                    post_result.removeComment(user_object_id,contents,function(err,result){
              
                        if(err){return err;}
                        
                        if(result===-1){
                            console.log('해당 코멘트를 삭제할 수 없습니다. 다시 한번 확인부탁드립니다.');
                            res.send({reaction:'해당 코멘트를 삭제할 수 없습니다. 다시 한번 확인부탁드립니다.'});
                            
                            return;
                            
                        }
                        
                        console.dir(result);

                        console.log("해당 코멘트 데이터가 삭제되었습니다.");

                        res.send({reaction:'해당 코멘트 데이터가 삭제되었습니다.'});

                    });
                }
            });       
        });
    } 
};

var ajax_update_post = function(req,res){
    console.log('/process/ajax_update_post 호출됨');
    
    var objectId = req.body.objectId;
    
    console.log('받은 정보 objectId :'+objectId);
    
    var database = req.app.get('database');
    
    if(database.db){
        database.PostModel.load(objectId,function(err,results){
            if(err){
                console.error('게시판 글 조회 중 오류 발생 :'+err.stack);
                
                res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                res.write('<h2>게시판 글 조회 중 오류 발생</h2>');
                res.write('<p>'+err.stack+'</p>');
                res.end();
                
                return;
            }
            
            if(results){
                console.dir(results);
                
                console.log('게시글 데이터를 축출했습니다.')
                
                res.send({result:results}); 
            }
        });
    }
    
};

var ajax_load_random_img = function(req,res){
    console.log('/process/ajax_load_random_img 호출됨');
    
    var database = req.app.get('database');
    
    if(database.db){
        database.PostModel.total_list(function(err,results){
            if(err){
                console.error('게시판 글 수집 중 오류 발생 :'+err.stack);
                
                res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                res.write('<h2>게시판 글 수집 중 오류 발생</h2>');
                res.write('<p>'+err.stack+'</p>');
                res.end();
                
                return;
            }
            
            if(results){
                
                let check_random = [];
                let result_random_img = [];
                let i = 0;
                
                console.dir(results);
                
                while(i<8){
                    let n = Math.floor(Math.random()*results.length);
                    
                    if(!sameNum(n)){
                      check_random.push(n);
                      result_random_img.push(results[n]); 
                        i++;
                    }
                    
                }
                
                function sameNum(n){
                    return check_random.find((e)=>(e===n));
                }
                 
                console.log('게시글 데이터를 축출했습니다.');
                
                res.send({results:result_random_img}); 
            }
        });
    }
};

var ajax_input_national_travel = function(req,res){
    console.log('/process/ajax_input_national_travel 호출됨');
    
    
    var data = {};
    
    if(req.session.user){
        console.log('세션정보가 확인되었습니다.');
        
        data={id:req.session.user.id,name:req.session.user.name};
        
        console.log(data);
        
    }else{
        
        console.log('세션정보가 없습니다.');
        
        data={id:null,name:null};
    }
    
    
    
    var database = req.app.get('database');
    
    if(database.db){
        database.PostModel.total_list(function(err,results){
            if(err){
                console.error('게시판 글 수집 중 오류 발생 :'+err.stack);
                
                res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                res.write('<h2>게시판 글 수집 중 오류 발생</h2>');
                res.write('<p>'+err.stack+'</p>');
                res.end();
                
                return;
            }
            
            if(results){
              
                console.log('게시글 데이터를 축출했습니다.');
                
                var result = {results:results,data:data}    
                
                res.send({results:result}); 
            }
            
        });
    }
    
};

var ajax_input_ready_to_travel_travel = function(req,res){
    console.log('/process/ajax_input_ready_to_travel_travel 호출됨');
    
    var data = {};
    
    if(req.session.user){
        console.log('세션정보가 확인되었습니다.');
        
        data={id:req.session.user.id,name:req.session.user.name};
        
        console.log(data);
        
    }else{
        
        console.log('세션정보가 없습니다.');
        
        data={id:null,name:null};
    }
    
    var database = req.app.get('database');
    
    if(database.db){
        database.PostModel.total_list(function(err,results){
            if(err){
                console.error('게시판 글 수집 중 오류 발생 :'+err.stack);
                
                res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                res.write('<h2>게시판 글 수집 중 오류 발생</h2>');
                res.write('<p>'+err.stack+'</p>');
                res.end();
                
                return;
            }
            
            if(results){
              
                console.log('게시글 데이터를 축출했습니다.');
                
                var result = {results:results,data:data}    
                
                res.send({results:result}); 
            }
            
        });
    }  
};

var ajax_input_vehicle_travel = function(req,res){
    console.log('/process/ajax_input_vehicle_travel 호출됨');
    
    var data = {};
    
    if(req.session.user){
        console.log('세션정보가 확인되었습니다.');
        
        data={id:req.session.user.id,name:req.session.user.name};
        
        console.log(data);
        
    }else{
        
        console.log('세션정보가 없습니다.');
        
        data={id:null,name:null};
    }
    
    var database = req.app.get('database');
    
    if(database.db){
        database.PostModel.total_list(function(err,results){
            if(err){
                console.error('게시판 글 수집 중 오류 발생 :'+err.stack);
                
                res.writeHead('200',{'Content-Type':'text/html;charset=utf8'});
                res.write('<h2>게시판 글 수집 중 오류 발생</h2>');
                res.write('<p>'+err.stack+'</p>');
                res.end();
                
                return;
            }
            
            if(results){
              
                console.log('게시글 데이터를 축출했습니다.');
                
                var result = {results:results,data:data}    
                
                res.send({results:result}); 
                
            }
            
        });
    }  
};


module.exports.ajax_response = ajax_response;
module.exports.ajax_random_img = ajax_random_img;
module.exports.ajax_sample = ajax_sample;
module.exports.ajax_logout = ajax_logout;
module.exports.ajax_exist_session = ajax_exist_session;
module.exports.ajax_comment_response = ajax_comment_response;
module.exports.ajax_comment_total_check = ajax_comment_total_check;
module.exports.ajax_comment_delete = ajax_comment_delete;
module.exports.ajax_update_post = ajax_update_post;
module.exports.ajax_load_random_img = ajax_load_random_img;
module.exports.ajax_input_national_travel = ajax_input_national_travel;
module.exports.ajax_input_ready_to_travel_travel = ajax_input_ready_to_travel_travel;
module.exports.ajax_input_vehicle_travel = ajax_input_vehicle_travel;