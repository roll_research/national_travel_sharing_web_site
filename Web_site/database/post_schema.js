var utils = require('../utils/utils');

var SchemaObj = { };

SchemaObj.createSchema = function(mongoose){
    
    var PostSchema = mongoose.Schema({
        title : {type:String, trim:true, 'default':''},
        contents : {type:String, trim:true, 'default':''},
        writer : {type:mongoose.Schema.ObjectId, ref:'users3'},
        info : {type:String, trim:true, 'default':''},
        region : {type:String, trim:true,'default':''},
        photo : {type:String, trim:true,'default':''},
        traffic : {type:String, trim:true,'default':''},
        tages : {type:[],'default':'',},
        created_at : {type:Date, index:{unique:false}, 'default':Date.now},
        updated_at : {type:Date, index:{unique:false}, 'default':Date.now},
        comments : [{
            contents : {type:String, trim:true, 'default':''},
            writer : {type:mongoose.Schema.ObjectId,ref:'users3'},
            created_at : {type:Date,'default':Date.now}
        }]
    });
    
    PostSchema.path('title').required(true,'글 제목을 입력하셔야 합니다.');
    PostSchema.path('contents').required(true,'글 내용을 입력하셔야 합니다.');
    
    PostSchema.methods = {
        savePost : function(callback){
            var self = this;
            
            this.validate(function(err){
                if(err) return callback(err);
                
                self.save(callback);
            });
        },
        
        addComment : function(user,comment,callback){
            this.comments.push({
                contents : comment.contents,
                writer : user._id
            });
            this.save(callback);
        },
        
        removeComment : function(id,contents,callback){
            var index = utils.indexOf(this.comments,{id:id,contents:contents});
            
            console.log('함수 사용 후 확인: '+index);
            
            if(index>=0){
                this.comments.splice(index,1);
            }else{
                //return callback('ID ['+id+']를 가진 댓글 객체를 찾을 수 없습니다.');
                return callback(null,-1);
            }
            
            this.save(callback);
        }
    }
    
    PostSchema.statics = {
        load : function(id, callback) {
            this.findOne({_id: id})
            .populate('writer','name id')
            .populate('comments.writer')
            .exec(callback);
        },
        total_list : function(callback){
            this.find({})
            .populate('writer','name id')
            .populate('comments.writer')
            .exec(callback);
        },
        list : function(options,callback) {
            var criteria = options.criteria || {};
            
            this.find(criteria)
                .populate('writer','name id')
                .sort({'created_at':-1})
                .limit(Number(options.perPage))
                .skip(options.perPage * options.page)
                .exec(callback);
        },
        delete : function(title,writer,callback){
            this.deleteOne({title:title,writer:writer})
            .exec(callback);
        },
        update: function(title,contents,info,objectId,region,photo,traffic,callback){
            this.findOneAndUpdate({_id:objectId},{$set:{title:title,contents:contents,info:info,region:region,photo:photo,traffic:traffic}},{returnNewDocument:true},function(err,result){
                if(err){
                    return callback(err,null);
                }
                console.log("수정 완료됨");
                return callback(null,result);
            }).populate('writer','name id');
        }
    }
    
    console.log('PostSchema 정의함');
    
    return PostSchema;
    
};

module.exports = SchemaObj;